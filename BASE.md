# Base

## Visual

### Textures, Models, & Lighting

- realvision enb | truevision enb
- enhanced lights and fx
- project parallax remastered
- skyrim hd - 2k textures
- amidianborn book of silence
- static mesh improvement mod
- footprints
- the ruffled feather : [Better Dynamic Snow, Argonian Access, Enhanced Distance Terrain]
- skyrim flora overhaul
- true storms - thunder and rain redone
- animations
- climates of tamriel
- skyfalls and skymills - animated distant waterfalls and windmills
- book covers skyrim
- smoking torches and candles
- immersive animations
- skyrim bigger trees
- real roads for skyrim
- splash of rain
- animated clutter
- skyrim project optimization (may be a part of elfx)
- realistic water two
- immersive interiors
- enhanced night skyrim

### Character Models & Textures
- unp
- unpb
- real girls realistic body texture for unp unpb and sevenbase
- wsco - windsong skyrim character overhaul
- superior lore friendly hair
- belt fastened quivers

## Audio

- Audio Overhaul for Skyrim 2
- Fantasy Soundtrack Project

## Immersion

- wet and cold (and ashes)
- multiple floors sandboxing
- interesting npcs
- better jumping
- get snowy
- immersive patrols
- immersive horses
- holidays
- extensible follower framework
- run for your lives
- realistic ragdolls and force (conflicts with unp)
- fire and ice overhaul
- open skyrim (resource intensive)
- face to face conversation
- quickdrop
- guard dialogue overhaul
- automatic variants
- clapping or applaud change
- skyrim - enhanced camera
- taverns aren't discotecs
- realistic boat bobbing
- abt - arrows and bolts tweaks
- follwer commentary overhaul
- move it dammit for npc companions and followers
- Multiple Floors Sandboxing
- immersive npc in the dark
- no snow under the roof
- think to yourself messages
- Immersive Player Dialogue
- Khajiit Speak
- NPC Bartering
- Farmers Sell Produce
- Mine Foremen Sell Ore
- wearable lanterns
- lanterns of skyrim - all in one
- Hold Border Banners
- Immersive Hold Borders
- Unique Border Gates
- telescope
- sit anywhere
- Go To Bed
- smart training
- binocular vision for npcs while sneaking
- skyrims unique treasures
- the 418th step
- craftable coins and ingots
- unlimited bookshelves
- better archery eagle eye perk

## Armoury

- immersive armors
- immersive weapons
- cloaks of skyrim
- winter is coming - cloaks
- apocalypse - magic of skyrim
- summer dress sets for unp by yurica
- spell charging
- unique uniques
- spell chaining (spell combos v2)
- lore weapon expansion
- hedge mage armor
- smart cast
- west wind combat series - assault armor unp unpb bbp
- balanced magic

## Cities

- expanded towns and cities
- legendary cities - tes arena - skyrim frontier fortress
- lakeview manor extended (only after upgrading lakeview)
- lakeview manor town and fort
- sjel blad castle
- sliverstead - a buildable dwemer estate
- elvenwood
- sky city - markarth rising
- underground bathhouse and paradise valley
- vellamo - mage home in winterhold
- breezehome fully upgradable
- helgen reborn
- whiterun mansion
- bank of markarth
- Immersive College of Winterhold
- overlook tower player home

## Game Mechanics

- skyrim redone
- perkus maximus
- frostfall
- ineed - food water and sleep
- Cobb Encumbrance
- re - real estate
- become a bard
- nock to tip
- become a skooma drug lord - skooming skyrim
- pirates of skyrim - the northern cardinal under the black flag
- trade routes
- Complete Fast Travel Overhaul 
- beggar's brawl arena
- alternate start - live another life
- master of disguise - immersive disguises for skyrim
- hearthfire multiple adoptions
- civil war overhaul
- familiar faces
- katixa's ciderhouse restaurant
- becomeJarlofIvarstead
- become high king of skyrim
- your market stall
- trade and barter
- guess the distance - perceptive scouting
- sneak tools
- archery gameplay overhaul

## Game Expansions

- minigame mod - the battle of heroes
- seb and cam retire to skyrim
- fireworks

## Patches

- unofficial skyrim patch
- onetweak
- cutting room floor

## UI

- skyui
- immersive hud
- skytweak
- racemenu
- even better quest objectives
- fxaa enb sweetfx manager
- less intrusive hud ii
- extended ui
- souls quick menu
- MainMenu
- skycomplete - automatically track quests - locations - books
- viewable faction ranks
- morehud
- iactivate
- atlas map markers for skyrim blackreach dawguard and dragonborn
- a matter of time - a hud clock widget
- 3d paper world map
- unique region names
- better message box controls
- better dialogue controls
- better sorting
- main font replacement
